<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;
    protected $fillable = [
        'fullname', 'nik', 'nip', 'job_position', 'place_of_birth', 'date_of_birth', 'gender'
    ];
}
