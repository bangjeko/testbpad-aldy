<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    //
    public function index()
    {
        $pegawai = Employee::all();
        return view('dashboard', ['pegawai' => $pegawai]);
    }

    public function add()
    {
        return view('addemployee');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'fullname' => 'required',
            'nik' => 'required',
            'date_of_birth' => 'required'
        ]);

        Employee::create([
            'fullname' => $request->fullname,
            'nik' => $request->nik,
            'nip' => $request->nip,
            'job_position' => $request->job_position,
            'place_of_birth' => $request->place_of_birth,
            'date_of_birth' => $request->date_of_birth,
            'gender' => $request->gender,
        ]);

        return redirect('/dashboard');
    }

    public function edit($id)
    {
        $pegawai = Employee::find($id);
        return view('editemployee', ['pegawai' => $pegawai]);
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'fullname' => 'required',
            'nik' => 'required',
            'date_of_birth' => 'required'
        ]);

        $pegawai = Employee::find($id);
        $pegawai->fullname = $request->fullname;
        $pegawai->nik = $request->nik;
        $pegawai->nip = $request->nip;
        $pegawai->job_position = $request->job_position;
        $pegawai->place_of_birth = $request->place_of_birth;
        $pegawai->date_of_birth = $request->date_of_birth;
        $pegawai->gender = $request->gender;
        $pegawai->save();
        return redirect('/dashboard');
    }

    public function delete($id)
    {
        $pegawai = Employee::find($id);
        $pegawai->delete();
        return redirect('/dashboard');
    }
}
