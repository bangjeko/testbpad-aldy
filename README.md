# Instalation
- Menggunakan Framework Laravel 8
- Php versi 7.4
- Database PostgreSql

###############################
# Konfigurasi Database .env

- DB_CONNECTION=pgsql
- DB_HOST=localhost
- DB_PORT=5432
- DB_DATABASE= testbpad
- DB_USERNAME=root
- DB_PASSWORD=root

###############################
- composer install
- php artisan migrate
- php artisan serve

###############################
- user admin : bangjeko@gmail.com
- password : 12345678

###############################