<?php

use App\Http\Controllers\EmployeeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', [EmployeeController::class, 'index'])->middleware(['auth'])->name('dashboard');
Route::get('/pegawai/add', [EmployeeController::class, 'add'])->middleware(['auth'])->name('pegawai-add');
Route::post('/pegawai/store', [EmployeeController::class, 'store'])->middleware(['auth'])->name('pegawai-store');
Route::get('/pegawai/edit/{id}', [EmployeeController::class, 'edit'])->middleware(['auth'])->name('pegawai-edit');
Route::post('/pegawai/update/{id}', [EmployeeController::class, 'update'])->middleware(['auth'])->name('pegawai-update');
Route::get('/pegawai/delete/{id}', [EmployeeController::class, 'delete'])->middleware(['auth'])->name('pegawai-delete');

require __DIR__.'/auth.php';
