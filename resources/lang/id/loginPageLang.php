<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    'rememberMe' => 'Ingat Saya',
    'wannaRegister' => 'Belum punya akun ?',
    'registerAccount' => 'Daftar Akun',
    'login' => 'Masuk',
    'register' => 'Daftar',
    'resenVerificationEmail' => 'Silahkan check email. Link Verifikasi telah dikirim pada alamat email yang anda isikan pada proses pendaftaran.',
    'thanksForRegister' => 'Terima kasih telah mendaftar. Sebelum login, silahkan verifikasi email anda dengan cara cek inbox email anda. Jika belum mendapatkan email, silahkan klik tombol Resend Verification Email dibawah.',
    'signInWithGoogle' => 'Masuk dengan akun Google',
    'orWithEmail' => 'atau dengan username/email',
    'forgotPassword' => 'Lupa Password ?',
    'forgotPasswordEnterEmail' => 'Masukkan email anda yang terdaftar pada sistem kami untuk melakukan reset password.',
    'submit' => 'Kirim',
    'pleaseWait' => 'Silahkan tunggu ...',
];
