<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'login' => 'Masuk',
    'setupNewPassword' => 'Pengaturan Password Baru',
    'alreadyResetPassword' => 'Sudah melakukan reset password ?',
    'passwordSuggestion' => 'Gunakan 8 sampai 16 karakter dengan kombinasi angka dan karakter',
    'submit' => 'Kirim',
    'emailNotExists' => 'Email :email tidak terdaftar di dalam sistem',
];
