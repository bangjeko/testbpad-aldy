<!--begin::User-->
<div class="d-flex align-items-center me-n3 ms-1 ms-lg-3" id="kt_header_user_menu_toggle">
    <!--begin::Menu wrapper-->
	<div class="btn btn-icon btn-active-light-primary btn btn-icon btn-active-light-primary btn-custom w-30px h-30px w-md-40px h-md-40px" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
		<img class="h-30px w-30px rounded" src="{{ asset('media/avatars/300-2.jpg') }}" alt="" />
	</div>
		<!--begin::User account menu-->
		<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-275px" data-kt-menu="true">
			<!--begin::Menu item-->
			<div class="menu-item px-3">
				<div class="menu-content d-flex align-items-center px-3">
					<!--begin::Avatar-->
					<div class="symbol symbol-50px me-5">
						<img alt="Logo" src="{{ asset('media/avatars/300-2.jpg') }}" />
					</div>
					<!--end::Avatar-->
					<!--begin::Username-->
					<div class="d-flex flex-column">
						<div class="fw-bold d-flex align-items-center fs-5">
                            {{ Auth::user()->username }}
                        </div>
						<a href="#" class="fw-semibold text-muted text-hover-primary fs-7">{{ Auth::user()->email }}</a>
					</div>
					<!--end::Username-->
				</div>
			</div>
			<!--end::Menu item-->
			<!--begin::Menu separator-->
			<div class="separator my-2"></div>
			<!--end::Menu separator-->
			<!--begin::Menu item-->
			<div class="menu-item px-5">
                <form method="POST" action="{{ route('logout') }}">
                   @csrf
	    		    <a href="route('logout')" class="menu-link px-5" onclick="event.preventDefault(); this.closest('form').submit();">{{ __('Sign Out') }}</a>
               </form>
			</div>
			<!--end::Menu item-->
		</div>
		<!--end::User account menu-->
	<!--end::Menu wrapper-->
</div>
<!--end::User -->