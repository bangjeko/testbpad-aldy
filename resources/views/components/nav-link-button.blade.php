@props(['classname'])

@php
$classes = $classname;
@endphp

<a {{ $attributes->merge(['class' => $classes]) }}>
    {{ $slot }}
</a>